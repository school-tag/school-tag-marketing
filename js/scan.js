/**
 * Allows anyone to view a player's status by scanning the URL in the Tag.
 * Scanning refers to the NFC NDEF URL in the tag or the URL written into the QR Code that can be read by a camera.
 * The NFC url is written by the school tag station at the player's school.
 * The QR Code url is written by the manufacturer of the school tag.
 * Each have a different key, but represent the same tag and the same player.
 * The manufacturer of the tag provides the mapping of the QR Code key to the Tag UID.
 * The database maps the QR Code key to the Tag UID, which is the value used to represent the player.
 * The TAG UID has a reference to the school the player owning the tag is participating.
 * This code will redirect to a School Tag Application's scan page.
 *
 * This seemingly complicated process is needed since the tags are manufactured without knowing destination school.
 */


/**
 * Constants used to guide the participants to success.
 */
const messages = {
    default: {
        title: "Have a player's tag?",
        content: "Enter the 4 character code below the QR Code to view the player's scoreboard.  Or scan it with a phone."
    },
    qr_code_not_found: {
        title: "Code Not Found",
        content: "The code entered is not from a valid School Tag.  Contact your Game Master or try again."
    },
    qr_code_not_mapped: {
        title: "Invalid Tag Setup",
        content: "The code is recognized, but the system setup is incomplete. Get a new tag from your Game Master."
    },
    tag_not_participating: {
        title: "Tag Not Registered",
        content: "Your tag is recognized, but it has not been registered with your school yet.  Contact your Game Master."
    },
    tag_not_found: {
        title: "Invalid Tag ID",
        content: "The ID of the tag is not found in the system. Get a new tag from your Game Master."
    },
};

/**The results of the scan or manual input.*/
class Scan {
    qrc;
    tagUid;
}

let db;

//loads db only if needed
function loadDb() {
    if (!db) {
        firebase.initializeApp({
            apiKey: "AIzaSyDI25aWeRQRTUyx04ghxaJgSh2ykhTqA5s",
            projectId: 'school-tag'
        });

        db = firebase.firestore();
    }
    return db;

}

/**
 * Looks for the TAG UID in the query parameter OR
 * Looks for the QRC Code which it will then hit the database to look for a matching Tag UID.
 *
 * @param {Scan} scan containing the qrc or tag uid
 * @return {Promise<string>} the tagUID or reject if it is not found
 */
function findTagUid(scan) {
    //find the tagUID
    if (scan.tagUid) {
        return Promise.resolve(scan.tagUid);
    } else if (scan.qrc) {
        return loadDb().collection("qrcodes").doc(scan.qrc).get().then((doc) => {
            if (doc.exists) {
                const tagUid = doc.data().uid;
                if (tagUid) {
                    scan.tagUid = tagUid;
                    return tagUid;
                } else {
                    return Promise.reject(messages.qr_code_not_mapped);
                }
            } else {
                return Promise.reject(messages.qr_code_not_found);
            }
        });
    } else {
        return Promise.reject("Coding Error:  You are passing in a scan without a qrc or tag uid!!!");
    }
}

/**
 * Given the tagUID, this will build the url to be redirected and return as promise
 * @param tagUid
 * @return {Promise<string>} the url to forward, reject with error otherwise
 */
function buildUrl(tagUid) {
    return loadDb().collection("tags").doc(tagUid).get().then((doc) => {
        if (doc.exists) {
            const data = doc.data();
            if (data.school) {
                return `https://${data.school}.schooltag.org/scan/backpack/tag?m=${tagUid}`
            } else {
                return Promise.reject(messages.tag_not_participating);
            }
        } else {
            return Promise.reject(messages.tag_not_found);
        }
    });
}


/**
 *
 * @param {Scan} scan containing the qrc, tag uid.
 */
function forwardToScoreboardIfFound(scan){
    //with the tag uid, forward to the participating school scan page
    findTagUid(scan).then((tagUid) => {
        scan.tagUid = tagUid;
        buildUrl(tagUid).then((url) => {
            window.location.replace(url);
        }).catch((error) => {
            showHelpMessage(error);
        });
    }).catch((error) => {
        showHelpMessage(error);
    });
}


/**
 * Given the text input, this will submit when 4 characters are entered.
 * Updates the input with upper case values.
 *
 * @param {input} textCodeInput
 */
function handleQrcInput(qrcInput) {
    const textCode = qrcInput.value.toUpperCase();
    qrcInput.value = textCode;
    showHelpMessage(messages.default);
    if(textCode.length === 4) {
        const scan = new Scan();
        scan.qrc = textCode;
        forwardToScoreboardIfFound(scan);
    }
}

/**The QR Code input text field that reflects the manually entered code.
 * @return {HTMLElement} the input field for the QR Code
 */
function qrcInput(){
    return  document.getElementById("qrc-input");
}

/**
 * Inspects the query parameters in the url, which are provided by the urls embedded in the NFC URL or QR Code.
 * If QR Code is scanned, then enter it in the input and use the handlers to explain the results.
 * If Tag UID is found, then forward to the scoreboard if found or use the default handlers.
 */
function handleQueryParams(){

    //the keys are extracted from query parameters, if any
    const queryParams = new URLSearchParams(window.location.search);
    const qrc = queryParams.get("qrc");
    if(qrc){
        const input = qrcInput();
        input.value = qrc;
        handleQrcInput(input);
    }else{
        const scan = new Scan();
        const tagUid = queryParams.get("tag");
        if(tagUid){
            scan.tagUid = tagUid;
            forwardToScoreboardIfFound(scan);
        }else{
            showHelpMessage(messages.default);
        }
    }
}


//enable popovers and tooltips
//https://getbootstrap.com/docs/4.0/components/popovers/
function showHelpMessage(message) {
    const input = $("#qrc-input");
    const popover = input.data('bs.popover');
    if(!popover){
        input.popover({ title: message.title , content: message.content })
        .click(function (){
            $(this).popover('show');
        })
        .blur(function () {
            $(this).popover('hide');
        });
        input.popover('show');
    }else{
        //https://stackoverflow.com/a/13565154/721000
        input.attr('data-original-title',message.title);
        input.attr('data-content',message.content);
        popover.setContent();
        popover.show();
    }

}

//execute the handling of query parameters if they are present
handleQueryParams();

//TODO: persist log in database to record view tags
