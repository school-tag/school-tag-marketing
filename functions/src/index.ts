import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';


admin.initializeApp(functions.config().firebase);


/**Receive notifications from the school tag cloud when a player scans to identify
 * which game the player is participating. The value being a parent scan will show
 * the player's school.
 *
 * TODO: don't just clobber the value, but keep a record of changing schools
 * TODO: writes could be reduced by reading first and writing only if different
 */
export const associateTag = functions.pubsub.topic('associate-tag').onPublish((message) => {

    console.log(`Received message: `, message.json);
    let db = admin.firestore();
    if (!message.json.uid) {
        return Promise.reject(`uid is missing from json ${message.json}`);
    }
    if (!message.json.school) {
        return Promise.reject(`school is missing from json ${message.json}`);
    }
    return db.collection('tags').doc(message.json.uid).set(message.json);
});