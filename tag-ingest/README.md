# School Tag Ingest

Correlating QR Codes to Tag UIDs.

## Ordering

* Purchase from [GoToTags](GoToTags.com)
* QR Codes need to be provided from [Spreadsheet](https://docs.google.com/spreadsheets/d/1mexFOlPnT_cerp5HbQLXkmlEoi-0idKR8RRRmhiyqlc/edit?usp=sharing)
  * Codes already used have a gray background
  * `https://schooltag.org?qrc={code}` is the url to receive the code
  * The code is written in plain text so the player can identify themselves without a device
* Place the order and request encoding
  * Encoding is required to receive Tag UID / QR Code correlation file
  * `https://schooltag.org?tag={UID}` is the encoding URL
  * Tags must be writeable and are NOT read only
* Receive the tags and verify QR Code and NFC works (inspecting the url for correctness)
* Save the correlation spreadsheet in [Google Drive](https://drive.google.com/drive/folders/1cbYwZoPMPR1hRLWfahF3fLEYD8ItjIlv) for safe keeping

## UID/Code Correlation

The system uses the Tag UID to play. The QR Code and Plain Text Code are provided 
for convenience of visual reading without an NFC reader.  The codes are arbitrary 
and correlation happens during manufacturing. 

Download the correlation file CSV:

* Open the correlation file provided during purchase
* Open the `Encoded Tags` tab
* File -> Download -> Comma separated values (.csv)
* That will produce a file named similar to `gototags encoding_O-2002680_encoded-3 - Encoded Tags.csv`
* We are only interested in the `Uid` and `Data2` (QR Code) columns 

Setup your system to run the python script against gcloud firestore:

* Admin access to [school-tag project](https://console.firebase.google.com/project/school-tag) is required
* Ensure [python3 is installed](https://realpython.com/installing-python/) 
* Install or update [gcloud](https://cloud.google.com/sdk/docs/downloads-interactive#mac)
* Authorize python to interact with gcloud using your credentials
  * `gcloud auth application-default`
* Choose project to modify
  * `gcloud config set project school-tag-sbox` to practice
  * `gcloud config set project school-tag` for real
Run the python script and update the firestore database:

* `python3 tags-to-firebase.py`
* Enter the path to the csv file downloaded
* Confirm output is error free
  * Warnings about `_CLOUD_SDK_CREDENTIALS_WARNING` is expected
* Confirm the entries made it to [firestore](https://console.firebase.google.com/project/school-tag/database/firestore)

Note: The script is idempotent and may be run multiple times with the same effect as long as the csv hasn't changed.

Validate tags are available for phone scanning:

* Open a Chrome browser on your desktop
* Open the developer tools window to the `console`
* Go to `https://schooltag.org?qrc={QR Code}` replace code with one you just ingested
* Confirm output shows expected UID
* Confirm output says `tag_not_found` 

This proves the QR Code is mapped to the UID, but the UID is not mapped to a school yet.  
The tag will be associated to a school when the tag is scanned at a school station.



