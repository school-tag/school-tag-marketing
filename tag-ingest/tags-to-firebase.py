import csv
import os
from google.cloud import firestore

csv_filename = input("Enter the path to the csv file:\n")

# set firestore to the schooltag.org where the mapping is done
# os.system('gcloud config set project school-tag') - set this prior. see instructions. practice first!!!!

db = firestore.Client()

# Then query for documents
users_ref = db.collection(u'users')

for doc in users_ref.stream():
    print(u'{} => {}'.format(doc.id, doc.to_dict()))

with open(csv_filename) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print(f'Column names are {", ".join(row)}')
            line_count += 1
            uid_index = row.index('Uid')
            qr_code_index = row.index('Data2')
            print(f'UID is column {uid_index}')
            print(f'QR Code is column {qr_code_index}')
            print('Writing to firestore...')
        else:
           uid=row[uid_index]
           if uid:
               qr_code=row[qr_code_index]
               print(f'{qr_code}={uid}')
               # write the qrc to the firestore mapping to the uid
               doc_ref = db.collection(u'qrcodes').document(f'{qr_code}')
               doc_ref.set({
                   u'uid': f'{uid}',
                   u'qrc': f'{qr_code}'
               })

               # do not write to the `tags` entity now. it will be mapped to a school when tagging in to school station

               line_count += 1
    print(f'{line_count -1} tags matched.')

